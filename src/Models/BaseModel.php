<?php

namespace WuriN7i\OneData\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

abstract class BaseModel extends EloquentModel
{
    protected $refNamespace;

    /**
     * {@inheritDoc}
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($region) {
            if (empty($region->ref)) {
                $region->ref = $region->generateRef();
            }
        });
    }

    public abstract function generateRef(): LazyUuidFromString;

    public function getRefNamespace(): LazyUuidFromString
    {
        $refNamespace = $this->refNamespace ?? $this->getTable();

        return Uuid::uuid5(Uuid::NAMESPACE_X500, $refNamespace);
    }

    public function scopeIsEnable(Builder $builder, $availability = true)
    {
        return $builder->where('is_enable', $availability);
    }
}