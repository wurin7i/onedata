<?php

namespace WuriN7i\OneData\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionSeeder::class,
            CivilDataSeeder::class,
        ]);
    }
}