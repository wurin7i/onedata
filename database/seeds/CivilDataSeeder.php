<?php

namespace WuriN7i\OneData\Seeders;

use WuriN7i\OneData\Models\Ethnic;
use WuriN7i\OneData\Models\Marital;
use WuriN7i\OneData\Models\Occupation;
use WuriN7i\OneData\Models\Religion;
use Illuminate\Database\Seeder as Seeder;

class CivilDataSeeder extends Seeder
{
    use HandleCsv;

    public function run()
    {
        $this->seedMaritals();
        $this->seedReligions();
        $this->seedEthnics();
        $this->seedOccupations();
    }

    protected function seedMaritals()
    {
        $maritals = [
            '1' => 'Belum Kawin',
            '2' => 'Kawin',
            '3' => 'Cerai Hidup',
            '4' => 'Cerai Mati',
        ];

        collect($maritals)
            ->map(function ($label, $code) {
                return [
                    'label' => $label, 'code' => $code, 'sort_order' => (int)$code,
                ];
            })
            ->mapInto(Marital::class)
            ->map(function ($item) {
                $item->save();
            });
    }

    protected function seedReligions()
    {
        $religions = [
            '1' => 'Islam',
            '2' => 'Kristen',
            '3' => 'Katholik',
            '4' => 'Hindu',
            '5' => 'Budha',
            '6' => 'Kong Hucu',
            '7' => 'Kepercayaan Terhadap Tuhan YME'
        ];

        collect($religions)
            ->map(function ($label, $code) {
                return [
                    'label' => $label, 'code' => $code, 'sort_order' => (int)$code,
                ];
            })
            ->mapInto(Religion::class)
            ->map(function ($item) {
                $item->save();
            });
    }

    protected function seedEthnics()
    {
        /**
         * @link https://id.wikipedia.org/wiki/Suku_bangsa_di_Indonesia
         */
        $ethnics = [
        //   Bangsa             Popul.  Persentase  Kawasan utama
            'Jawa', //          95,2    40,45       Jawa Timur, Jawa Tengah, Yogyakarta, Lampung
            'Sunda', //         36,7    14,5        Jawa Barat
            'Batak', //         8,5     3,58        Sumatra Utara, Riau
            'Madura', //        7,2     3,03        Pulau Madura
            'Betawi', //        6,8     2,88        Jakarta
            'Minangkabau', //   6,5     2,73        Sumatra Barat, Riau
            'Bugis', //         6,3     2,69        Sulawesi Selatan
            'Melayu', //        5,3     2,27        Sumatra dan Kalimantan; terutama di Jambi, Bengkulu, Sumatra Selatan, Lampung, Sumatra Timur, Riau, Kepulauan Riau, Bangka-Belitung, dan Kalimantan Barat
            'Arab', //          5,0     2,10        Jakarta, Jawa Barat, Jawa Tengah, Jawa Timur, Kalimantan, dan Sumatra
            'Banten', //        4,6     1,97        Banten
            'Banjar', //        4,1     1,74        Kalimantan Selatan
            'Bali', //          3,9     1,67        Pulau Bali
            'Sasak', //         3,1     1,34        Pulau Lombok, Pulau Sumbawa
            'Dayak', //         3,0     1,27        Pulau Kalimantan
            'Tionghoa', //      2,8     1,20        Sumatra, Jawa, Bali, Kalimantan, Sulawesi
            'Makassar', //      2,7     1,13        Sulawesi Selatan
            'Kaili', //         2,6     1,5         Sulawesi tengah
            'Cirebon', //       1,9     0,79        Jawa Barat
        ];

        collect($ethnics)
            ->map(function ($label, $key) {
                $code = $key + 1;
                return [
                    'label' => $label, 'code' => $code, 'sort_order' => $code,
                ];
            })
            ->mapInto(Ethnic::class)
            ->map(function ($item) {
                $item->save();
            });
    }

    protected function seedOccupations()
    {
        $path = __DIR__.'/csv/occupations.csv';

        $this->handleCsvFile($path, function ($data) {
            Occupation::create([
                'code' => $data[0],
                'label' => $data[1],
                'sort_order' => (int)$data[0],
            ]);
        });
    }
}