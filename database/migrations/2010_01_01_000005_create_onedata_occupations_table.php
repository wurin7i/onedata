<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnedataOccupationsTable extends Migration
{
    public function up()
    {
        Schema::create('onedata_occupations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->uuid('ref')->unique();
            $table->char('code', 2);
            $table->string('label');
            $table->integer('sort_order');
            $table->boolean('is_enable')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('onedata_occupations');
    }
}
