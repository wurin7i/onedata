<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnedataRegionsTable extends Migration
{
    public function up()
    {
        Schema::create('onedata_regions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->uuid('ref')->unique();
            $table->string('label');
            $table->string('bps_code')->unique()->nullable();
            $table->string('name');
            $table->smallInteger('level')
                ->comment('0: country, 1: provinces, 2: regencies/cities, 3: subdistricts / districts, 4: villages');
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('is_enable')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('onedata_regions');
    }
}
